console.log("Hello, world!");
console. log ("Hello, Batch 203!")
console.
log
(
	"Hello, everyone!"
)

// Comments:

// This is a single line comment. ctrl + /
/*
	This is a
	Multi line comment
	ctrl + shft + /
*/

// Syntax and Statements

// Statements in programming are instructions that we tell the computer to perform

// Syntax in programming, it is the set rules that describes how statements must be constructed.

// Variables
/*
	- it is used to contain data

	- Syntax in declaring variables
	- let/const variableNameSample; 
*/

let myVariable = "Hello";

console.log(myVariable);

// console.log(hello); // will result to not defined error

// let hello; // using variables before they're declared will also return an error.

/*
	Guides in writing variables:
	    1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
	    2. Variable names should start with a lowercase character, use camelCase for multiple words.
	    3. For constant variables, use the 'const' keyword.
	    4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.
	    5. Never name a variable starting with numbers
*/

/* Declaring a variable with initial value
	let/const variableName = value;
*/

let productName = 'desktop computer';
console.log(productName);

let product = "Alvin's computer";
console.log(product);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;

// let - we usually change/reassign the values in our variable

// Reassingning variable values
// Syntax
	// variableName = newValue;

productName = "Laptop";
console.log(productName);

let friend = "Kate";
friend = "Jane";
console.log(friend); // error: Identifier "friend" has already been declared.

// interest = 4.489;
console.log(interest); //error because of assigning a value to a constant variable.

// Reassigning variables vs initializing variables.
//Declare ng variable
let supplier;
//  Initialization is done after the variables has been declared.
supplier = "John Smith Tradings";
console.log(supplier);

// Reassignment of variable because it's initial value was already declared.
supplier = "Zuitt Store";
console.log(supplier);

const pi = 3.1416;
// pi = 3.1416;
console.log(pi); //error due to const initialization

// var(ES1) vs let/const(ES6)
// let/const keyword avoid the hoisting of variables.
// a = 5;
// console.log(a);
// var a

// Multiple variable declarations
let productCode = "DC017";
const productBrand = "Dell";
console.log(productCode, productBrand);

// Using a variable with a reserved keyword.
// const let = "hello";
// console.log(let); // error: cannot used reserved keyword as a variable name.

// [SECTION] Data Types

// Strings
// Strings are series of characters that create a word, a phrase, a sentence or anything related to creating text.
//  Strings in JavaScript a single ('') or double ("") quote.
let country = 'Philippines';
let province = "Metro Manila";

// Concatenation Strings
// Multiple string values can be combined to create a single string using the "+" symbol.
let fullAddress = province + ', ' + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

//  Escape character(\)
// "\n" refers to creatng new line or set the text to next line.
let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress);

let message =  "John's employees went home early.";
console.log(message);

message = 'John\'s employees went home early';
console.log(message);

// Numbers
// Integers/Whole Numbers
let headcount = 26;
console.log(headcount);

// Decimal Numbers/Fractions
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combine number and strings
console.log("John's grade last quarter is " + grade);

// Boolean
// true/false
let isMarried = false;
let isGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("isGoodConduct: "+ isGoodConduct);

// Arrays
// it is used to store multiple values with similar data type.
// Syntax:
	// let/const arrayName = [elementA, elementB, elementC, ....]
	let grades = [98.7, 92.1, 90.2, 94.6];
	console.log(grades);

	// different data types
	// Storing  different data types inside an array is not recommended because it will not make sense to in the context of programming.
	let details = ["John", "Smith", 32, true];
	console.log(details);

// Objects
// Objects are another special kind of data type that's used to mimic real world objects/items.
// Syntax
	/*
		let/const objectName = {
			propertyA: value,
			propertyB: value
		}
	*/

	let person = {
		fullName: "Juan Dela Cruz",
		age: 35,
		isMarried: false,
		contact: ["+63917 123 4567", "8123 4567"],
		address: {
			houseNumber: "345",
			city: "Manila"
		}
	}

	console.log(person);

	// Create abstract object
	const myGrades = {
		firstGrading: 98.7,
		secondGrading: 92.1,
		thirdGrading: 90.2,
		fourthGrading: 94.6
	}
	console.log(myGrades);

	// typeof operator is used to determine the type of data or the variable value.
	console.log(typeof myGrades);

	console.log(typeof	grades);

	// Constant Objects and Arrays
	// We cannot reassign the value of the variable, but we can change the elements of the constant array.
	const anime = ["one piece", "one punch man", "attack on titan"];
	console.log(anime)
	anime[0] = "kimetsu no yaiba";

	console.log(anime);

	// Null
	// It is used to intentionally express the absence of the value in a variable declaration/initialization.
	let spouse = null;
	console.log(spouse);

	let myNumber = 0; //number
	let myString = ""; //string

	// Undefined
	// Represents the state of a variable that has been declared but without an assigned value.
	let fullName;
	console.log(fullName);

	// Undefined vs Null
	// One clear difference between undefined and null is that for undefined, a variable was created but was not provided a value
	// null means that a variable was created and was assigned a value that does not hold any value/amount
	// Certain processes in programming would often return a "null" value when certain tasks results to nothing
